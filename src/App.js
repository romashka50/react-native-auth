import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';

import { Header, Button, Spinner, Card, CardSection } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {
  state = {
    loggedIn: null,
  };

  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyCeB7dYLypoHNJSs4W_lePZB1XMLAZh0wk',
      authDomain: 'auth-99cb7.firebaseapp.com',
      databaseURL: 'https://auth-99cb7.firebaseio.com',
      projectId: 'auth-99cb7',
      storageBucket: 'auth-99cb7.appspot.com',
      messagingSenderId: '713901839273'
    });
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
        return (
          <Card >
            <CardSection>
              <Button onPress={() => firebase.auth().signOut()}>
                Log out
              </Button>
            </CardSection>
          </Card >
        );
      case false:
        return <LoginForm />;
      default:
        return (
          <View style={styles.spinnerStyle}>
            <Spinner />
          </View>
        );
    }
  }

  render() {
    return (
      <View >
        <Header headerText="Authentication!" />
        {this.renderContent()}
      </View >
    );
  }
}

const styles = {
  spinnerStyle: {
    height: '100%'
  }
};

export default App;
