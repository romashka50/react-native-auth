import React, { Component } from 'react';
import { Text } from 'react-native';
import firebase from 'firebase';

import {
  Card,
  CardSection,
  Button,
  Input,
  Spinner
} from './common';

class LoginForm extends Component {
  state = {
    email: '',
    password: '',
    loading: false,
  };

  onPress() {
    const { email, password } = this.state;

    this.setState({ error: '', loading: true });

    firebase.auth()
      .signInWithEmailAndPassword(email, password)
      .then(this.onLoginSucess.bind(this))
      .catch(() => {
        firebase.auth()
          .createUserWithEmailAndPassword(email, password)
          .then(this.onLoginSucess.bind(this))
          .catch(this.onLoginFail.bind(this));
      });
  }

  onLoginFail() {
    this.setState({ error: 'Authentication failed.', loading: false });
  }

  onLoginSucess() {
    this.setState({
      error: '',
      email: '',
      password: '',
      loading: false
    });
  }

  renderButton() {
    if (this.state.loading) {
      return <Spinner size="small" />;
    }

    return (
      <Button onPress={this.onPress.bind(this)}>
        Log In
      </Button>
    );
  }

  render() {
    return (
      <Card >
        <CardSection>
          <Input
            placeholder="user@dot.com"
            label="Email"
            value={this.state.email}
            onChangeText={email => this.setState({ email })}
          />
        </CardSection>
        <CardSection>
          <Input
            secureTextEntry
            placeholder="password"
            label="Password"
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
          />
        </CardSection>
        <Text style={styles.textErrorStyle}>
          {this.state.error}
        </Text>
        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card >
    );
  }
}

const styles = {
  textErrorStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red',
  }
};

export default LoginForm;
